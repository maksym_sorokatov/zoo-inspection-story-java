package com.epam.engx.story.zoo.external;

public interface AnimalStatus {

    boolean isAnimalSick();
}
